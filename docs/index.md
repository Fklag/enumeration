# Énumération

> Quelques problèmes de dénombrement, dont l'énoncé est simple, mais dont la résolution peut faire intervenir des méthodes variées en mathématiques et en algorithmique.

- Plusieurs méthodes sont proposées, avec toujours un code en *Python*. Les bonnes pratiques de programmation sont mises en avant, avec, en particulier, une première approche par force brute.
- Les méthodes avancées nécessitent des connaissances en algèbre linéaire ou en arithmétique, et sont destinées au public d'enseignants de mathématiques, ou d'étudiants.


test +